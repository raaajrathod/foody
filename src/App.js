import React, {useEffect, Fragment} from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import Navbar from "./components/layouts/Navbar";
import Footer from "./components/layouts/Footer";
import Home from "./components/pages/Home";
import Breakfast from "./components/pages/Breakfast";
import Lunch from "./components/pages/Lunch";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import "./materialize.css";
import "./App.css";

function App() {
  useEffect(() => {
    M.AutoInit();
    // estlint-disable-next-line
  }, []);

  return (
    <Router>
      <Navbar />
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/breakfast' component={Breakfast} />
        <Route exact path='/lunch' component={Lunch} />
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
