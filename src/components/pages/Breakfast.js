import React, {Fragment} from "react";
import Slider from "../layouts/breakfast/Slider";
import SectionOne from "../layouts/home/SectionOne";
import Menu from "../layouts/breakfast/Menu";
import Items from "../layouts/breakfast/Items";

const Breakfast = () => {
  return (
    <Fragment>
      <Slider />
      <SectionOne />
      <Menu />
      <Items />
    </Fragment>
  );
};

export default Breakfast;
