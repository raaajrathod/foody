import React, {Fragment} from "react";
import PropTypes from "prop-types";
import Landing from "../layouts/home/Landing";
import SectionOne from "../layouts/home/SectionOne";
import Category from "../layouts/home/Category";
import OurStory from "../layouts/home/OurStory";

const Home = props => {
  return (
    <Fragment>
      <Landing />
      <SectionOne />
      <Category />
      <OurStory />
    </Fragment>
  );
};

export default Home;
