import React, {Fragment} from "react";
import LunchSlider from "../layouts/lunch/LunchSlider";
import SectionOne from "../layouts/home/SectionOne";
import Tabs from "../layouts/lunch/Tabs";

const Lunch = () => {
  return (
    <Fragment>
      <LunchSlider />
      <SectionOne />
      <Tabs />
    </Fragment>
  );
};

export default Lunch;
