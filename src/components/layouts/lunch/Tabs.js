import React, {useEffect} from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import ItemCard from "../breakfast/ItemCard";

const Tabs = () => {
  useEffect(() => {
    var elems = document.querySelectorAll(".tabs");
    var instance = M.Tabs.init(elems, {});
  }, []);
  return (
    <div className='container'>
      <div className='row'>
        <div className='col s12 m12 l12 center-align'>
          <h2>Our Lunch Menu</h2>
          <span>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Id qui
            officia explicabo veritatis aut non!
          </span>
        </div>
      </div>
      <div className='row'>
        <div className='col s12'>
          <ul className='tabs'>
            <li className='tab col s4'>
              <a href='#test1'>Starter</a>
            </li>
            <li className='tab col s4'>
              <a className='active' href='#test2'>
                Main Course
              </a>
            </li>
            <li className='tab col s4'>
              <a href='#test4'>Dessert</a>
            </li>
          </ul>
        </div>
        <div id='test1' className='col s12'>
          <div className='row'>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
          </div>
        </div>
        <div id='test2' className='col s12'>
          <div className='row'>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
          </div>
        </div>
        <div id='test4' className='col s12'>
          <div className='row'>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tabs;
