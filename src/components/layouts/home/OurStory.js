import React, {Fragment} from "react";
import ourStoryImg from "../../../img/ourStory.jpg";

const OurStory = () => {
  return (
    <Fragment>
      <div className='container'>
        <div className='row'>
          <div className='col s12 m12 l12 center-align'>
            <h2>Our Story...</h2>
          </div>
        </div>
        <div className='row'>
          <div className='container'>
            <div className='col s12 m12 l6'>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Saepe,
                ullam repellat hic, nostrum ipsam veniam rerum sapiente facere
                dignissimos quasi non autem, quod labore quos? Quas voluptatibus
                a tempora corrupti quisquam sed repellat eius provident cum, aut
                alias, hic praesentium dolore illo blanditiis ea ullam in sint
                iure inventore reprehenderit ipsa tempore dolores. Quisquam
                aliquam eaque voluptates commodi, suscipit quibusdam? Debitis,
                quia tenetur. Ducimus, quos iusto laborum sapiente earum
                doloremque odit praesentium voluptate nisi excepturi maiores
                officiis alias voluptatum, adipisci, cupiditate laudantium odio
                iste totam perferendis similique dolorum illum. Veritatis
                placeat consequatur cum vel ex nesciunt id. Illo earum saepe
                itaque? Assumenda laudantium corporis non quas ipsum ad. Ut
                pariatur sed dolorem dicta adipisci aliquid aperiam, atque
                laudantium fuga, eveniet quas ea praesentium eum perferendis id
                repellat commodi libero facilis culpa placeat iste hic
                accusantium optio esse. Accusantium ea at harum, numquam dolor
                incidunt ducimus maiores molestias natus officiis! Quas.
              </p>
            </div>
            <div className='col s12 m12 l6'>
              <img
                src={ourStoryImg}
                alt=''
                style={{width: "100%", height: "350px"}}
              />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default OurStory;
