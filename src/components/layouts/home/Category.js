import React, {Fragment} from "react";
import breakfast from "../../../img/breakfast.jpg";
import lunch from "../../../img/lunch.jpg";
import dinner from "../../../img/dinner.jpg";
import {Link} from "react-router-dom";

const Category = () => {
  return (
    <Fragment>
      <div className='container'>
        <div className='row'>
          <div className='col s12 l12 m12 center-align'>
            <h2>Our Categories</h2> <br />
            <span>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum,
              corrupti fuga animi dolor qui quidem!
            </span>
          </div>
        </div>
        <div className='row'>
          <div className='container'>
            <div className='col s12 m12 l4'>
              <img
                src={breakfast}
                alt=''
                style={{width: "100%", height: "229px"}}
              />
              <br />
              <h5>Breakfast</h5>
              <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam
                ab neque et hic eum incidunt?
              </p>{" "}
              <Link to='/breakfast' className='btn'>
                View Menu
              </Link>
            </div>
            <div className='col s12 m12 l4 marginTopOnMobile'>
              <img
                src={lunch}
                alt=''
                style={{width: "100%", height: "229px"}}
              />
              <br />
              <h5>Lunch</h5>
              <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam
                ab neque et hic eum incidunt?
              </p>{" "}
              <Link to='/lunch' className='btn'>
                View Menu
              </Link>
            </div>
            <div className='col s12 m12 l4 marginTopOnMobile'>
              <img
                src={dinner}
                alt=''
                style={{width: "100%", height: "229px"}}
              />
              <br />
              <h5>Dinner</h5>
              <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam
                ab neque et hic eum incidunt?
              </p>{" "}
              <Link to='#!' className='btn'>
                View Menu
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Category;
