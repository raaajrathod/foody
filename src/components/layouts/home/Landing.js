import React from "react";

const Landing = props => {
  return (
    <section className='landing'>
      <div className='dark-overlay'>
        <div className='landing-inner'>
          <h1>Where Tastes Meets Myth</h1>
          <p className='grey-text'>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatem
            blanditiis porro neque obcaecati quae inventore libero iusto iste
            aliquid, nam modi asperiores rem assumenda incidunt sit velit fuga
            sed quidem?
          </p>
        </div>
      </div>
    </section>
  );
};

export default Landing;
