import React from "react";
import {Link} from "react-router-dom";

const Navbar = () => {
  return (
    <div className='navbar-fixed'>
      <nav>
        <div className='nav-wrapper container'>
          <Link to='/' className='brand-logo'>
            Brand Name{" "}
          </Link>
          <Link
            to='#!'
            className='hide-on-med-and-down navbar-social-links'
            style={{fontSize: "140%"}}>
            |
          </Link>
          <Link
            to='#!'
            className='hide-on-med-and-down'
            style={{marginLeft: "2.5%"}}>
            <i className='fab fa-twitter' />{" "}
          </Link>

          <Link
            to='#!'
            className='hide-on-med-and-down'
            style={{marginLeft: "1%"}}>
            <i className='fab fa-instagram' />{" "}
          </Link>
          <Link to='#' data-target='mobile-demo' className='sidenav-trigger'>
            <i className='material-icons'>menu</i>
          </Link>
          <ul className='right hide-on-med-and-down'>
            <li>
              <Link to='/breakfast' className='black-text'>
                Breakfast
              </Link>
            </li>
            <li>
              <Link to='/lunch'>Lunch</Link>
            </li>
            <li>
              <Link to='collapsible.html'>Dinner</Link>
            </li>
            <li>
              <Link to='mobile.html'>About Us</Link>
            </li>
          </ul>
        </div>
      </nav>

      <ul className='sidenav' id='mobile-demo'>
        <li>
          <Link to='#!'>
            <h3>Brand Name</h3>{" "}
          </Link>
        </li>
        <li>
          <Link to='/breakfast'>
            <h5>Breakfast</h5>
          </Link>
        </li>
        <li>
          <Link to='/lunch'>
            <h5>Lunch</h5>
          </Link>
        </li>
        <li>
          <Link to='collapsible.html'>
            <h5>Dinner</h5>
          </Link>
        </li>
        <li>
          <Link to='mobile.html'>
            <h5>About Us</h5>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default Navbar;
