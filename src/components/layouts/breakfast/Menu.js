import React, {Fragment} from "react";

const Menu = () => {
  return (
    <Fragment>
      <div className='container'>
        <div className='row'>
          <div className='container'>
            <div className='col s12 m12 l12 center-align'>
              <h2>Breakfast Menu</h2>
              <span>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Provident, id?
              </span>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Menu;
