import React, {Fragment} from "react";
import ItemCard from "./ItemCard";

const Items = () => {
  return (
    <Fragment>
      <div className='container'>
        <div className='row'>
          <div className='container1'>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
            <div className='col s12 m12 l4'>
              <ItemCard />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Items;
