import React, {useEffect} from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import Slider_1 from "../../../img/breakfast/slider_1.jpg";
import Slider_2 from "../../../img/breakfast/slider_2.jpg";
import Slider_3 from "../../../img/breakfast/slider_3.jpg";
import Slider_4 from "../../../img/breakfast/slider_4.jpg";

const Slider = () => {
  useEffect(() => {
    var elems = document.querySelectorAll(".slider");
    var instances = M.Slider.init(elems, {
      indicators: false,
      height: 450,
      interval: 10000,
      duration: 1500
    });
  }, []);

  return (
    <div className='slider'>
      <ul className='slides'>
        <li>
          <img src={Slider_1} />
          <div className='caption center-align'>
            <h3>This is our big Tagline!</h3>
            <h5 className='light grey-text text-lighten-3'>
              Here's our small slogan.
            </h5>
          </div>
        </li>
        <li>
          <img src={Slider_2} />
          <div className='caption left-align'>
            <h3>Left Aligned Caption</h3>
            <h5 className='light grey-text text-lighten-3'>
              Here's our small slogan.
            </h5>
          </div>
        </li>
        <li>
          <img src={Slider_3} />
          <div className='caption right-align'>
            <h3>Right Aligned Caption</h3>
            <h5 className='light grey-text text-lighten-3'>
              Here's our small slogan.
            </h5>
          </div>
        </li>
        <li>
          <img src={Slider_4} />
          <div className='caption center-align'>
            <h3>This is our big Tagline!</h3>
            <h5 className='light grey-text text-lighten-3'>
              Here's our small slogan.
            </h5>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default Slider;
