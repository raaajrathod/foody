import React, {Fragment} from "react";
import breakfast from "../../../img/breakfast.jpg";

const ItemCard = () => {
  return (
    <Fragment>
      <div className='card sticky-action'>
        <div className='card-image waves-effect waves-block waves-light'>
          <img
            className='activator'
            src={breakfast}
            style={{height: 200, objectFit: "cover"}}
          />
        </div>
        <div className='card-content' style={{paddingBottom: 0}}>
          <span className='card-title activator grey-text text-darken-4'>
            <h5
              style={{
                display: "inline-block",
                marginTop: 0,
                textAlign: "center"
              }}>
              {" "}
              Item Name
            </h5>
            <i className='material-icons right'>more_vert</i>
          </span>
          <hr />
          <div className='row noBottomMargin '>
            <div className='col s4 center-align'>
              <i className='material-icons'>person</i>
              <br />
              <h5>&#x20b9; 100</h5>
            </div>
            <div className='col s4 center-align'>
              <i className='material-icons'>people</i>
              <br />
              <h5>&#x20b9; 200</h5>
            </div>
            <div className='col s4 center-align'>
              <i className='material-icons'>group_add</i>
              <br />
              <h5>&#x20b9; 300</h5>
            </div>
          </div>
        </div>

        <div className='card-action hide-on-med-and-down'>
          <div className='row noBottomMargin'>
            <div className='col s4'>
              <button className='btn col s12 l12'>
                <i className='material-icons'>add</i>
              </button>
            </div>
            <div className='col s4'>
              <input
                type='number'
                className='col s12 l12'
                value='1'
                style={{textAlign: "center"}}
                onChange={e => console.log(e.target.value)}
              />
            </div>
            <div className='col s4'>
              <button className='btn col s12 l12'>
                <i className='material-icons'>remove</i>
              </button>
            </div>
          </div>
          <div className='row noBottomMargin'>
            <div className='col s12 m12 l12'>
              <button className='btn col s12 l12'>Add to Cart</button>
            </div>
          </div>
        </div>

        <div className='card-reveal'>
          <span className='card-title grey-text text-darken-4'>
            Card Title<i className='material-icons right'>close</i>
          </span>
          <p>
            Here is some more information about this product that is only
            revealed once clicked on.
          </p>
        </div>
      </div>
    </Fragment>
  );
};

export default ItemCard;
