import React from "react";
import sectionOneImage from "../../../img/sectionOne.jpg";

const SectionOne = () => {
  return (
    <div className='container'>
      <div className='row sectionOneRow'>
        <div className='container'>
          <div className='col s12 m6 l6'>
            <img
              src={sectionOneImage}
              alt=''
              className='image-responsive'
              style={{width: "100%"}}
            />
          </div>
          <div className='col s12 m6 l6 sectionOneText'>
            <h3> High Quality Ingredients for great taste</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia
              repellendus optio minima numquam corrupti veniam nesciunt
              voluptatem odio alias fuga.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SectionOne;
